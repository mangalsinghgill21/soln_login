﻿using Dal.ORM.Concrete;
using Dal.ORM.Interface.IConcrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.ORM.Factory
{
   public static class FactoryConcrete
    {
        #region enum
        public enum ConcreteType
        {
            Userlogin = 0
        };
        #endregion

        #region Declaration
        private static Dictionary<ConcreteType, dynamic> _dicObj = new Dictionary<ConcreteType, dynamic>();
        #endregion

        #region Constructor
        static FactoryConcrete()
        {
            AddConcreteInstance();
        }
        #endregion

        #region Private Method
        private static void AddConcreteInstance()
        {
            _dicObj.Add(ConcreteType.Userlogin, new Lazy<Iuserconcrete>(() => userconcrete.CreateInstance.Value));
        }
        #endregion

        #region Public Method
        public static TConcrete ExecuteFactory<TConcrete>(ConcreteType concreteTypeObj) where TConcrete : class
        {
            return _dicObj[concreteTypeObj].Value as TConcrete;
        }
        #endregion 
    }
}
