﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.ORM.Interface
{
    public interface ISetRepository<TEntity> where TEntity : class
    {
        Task<dynamic> SetDataAsync(string command, TEntity entity, Action<int?, string> storedProcOutPara = null);
    }
}
