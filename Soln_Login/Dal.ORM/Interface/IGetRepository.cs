﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.ORM.Interface
{
    public interface IGetRepository<TEntity, TLinqEntity>
        where TEntity : class
        where TLinqEntity : class
    {
        Task<dynamic> GetDataAsync(string command, TEntity entityObj, Func<TLinqEntity, TEntity> selectQuery, Action<int?, String> actionStoredProcOutPara = null);
    }
}
