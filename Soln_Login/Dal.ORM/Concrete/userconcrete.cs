﻿using Dal.ORM.Interface.IConcrete;
using Dal.ORM.ORD.UserLogin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserEntity.Interface;

namespace Dal.ORM.Concrete
{
    internal class userconcrete : Iuserconcrete
    {
        #region Declaration
        // create an instance of userlogindb Data Context
        private Lazy<UserLoginDataContext>  db = null;

        private static Lazy<userconcrete> userconcreteobj = null;

        #endregion

        #region Constructor
        private userconcrete()
        {

        }
        #endregion

        #region Property
        private Lazy<UserLoginDataContext> DbInstance
        {
            get
            {
                return
                       db ??
                            (db = new Lazy<UserLoginDataContext>(() => new UserLoginDataContext()));
            }
        }

        public static Lazy<userconcrete> CreateInstance
        {
            get
            {
                return
                       userconcreteobj
                            ??
                                (userconcreteobj = new Lazy<userconcrete>(() => new userconcrete()));

            }
        }

        #endregion

        #region Public Method
        public void Dispose()
        {
            db.Value.Dispose();
            db = null;

            userconcreteobj = null;
        }

        public async Task<dynamic> SetDataAsync(string command, Iuserentity entity, Action<int?, string> storedProcOutPara = null)
        {
            int? status = null;
            string message = null;
           try
            {
                return  await Task.Run(() =>
                {
                    var setQuery =
                        DbInstance
                        .Value
                        .uspSetUserLogin
                        (
                            command,
                            entity.Userid,
                            entity.Username,
                            entity.Password,
                            entity.Cookies,
                           entity.Token,
                           ref status,
                           ref message
                          );


                    storedProcOutPara(status, message);

                    return setQuery;    

                });
            }
            catch(Exception)
            {
                throw;
            }
        }

        //public async Task<dynamic> GetDataAsync(string command, Iuserentity entityobj, Func< Iuserentity> selectQuery, Action<int?, string> actionStoredProcOutPara = null)
        //{
        //    int? status = null;
        //    string message = null;
        //    try
        //    {
        //        return await Task.Run(() => {

        //            var getQuery =
        //                DbInstance
        //                .Value
        //                .uspSetUserLogin(
        //                    command,
        //                    entityobj.Userid,
        //                    entityobj.Username,
        //                    entityobj.Password, 
        //                    entityobj.Token,
        //                    entityobj.Cookies,
        //                    ref status,
        //                    ref message
        //                    )
        //                .AsEnumerable()
        //                .Select(selectQuery)
        //                .ToList();

        //            return getQuery;
        //        });
        //    }
        //    catch(Exception)
        //    {
        //        throw;
        //    }
        //}
        #endregion
    }
}
