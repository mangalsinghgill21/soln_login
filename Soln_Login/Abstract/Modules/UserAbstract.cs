﻿using UserEntity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract.Modules
{
  public abstract class UserAbstract : IDisposable
    {
      public abstract void Dispose();
      public abstract Task<dynamic> UserLogin(Iuserentity userentity);
   
  }

}
