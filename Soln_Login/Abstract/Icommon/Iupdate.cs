﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract.Icommon
{
    public interface Iupdate<TEntity> where TEntity : class
    {
        Task<dynamic> UpdateAsync(TEntity entityObj);
    }
}
