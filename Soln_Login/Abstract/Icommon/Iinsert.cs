﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract.Icommon
{
    public interface Iinsert<TEntity> where TEntity : class
    {
        Task<dynamic> AddAsync(TEntity entityObj);
    }
}
