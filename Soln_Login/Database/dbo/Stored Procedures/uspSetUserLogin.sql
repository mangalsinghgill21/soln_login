﻿Create PROCEDURE [dbo].[uspSetUserLogin]
	@Command Varchar(50)=NULL,
	
	@UserId Numeric(18,0)=NULL,
	@UserName Varchar(50)=NULL,
	@Password Varchar(50)=NULL,
	@Cookies nvarchar(max)=null,
	@Token nvarchar(max)=null,
	@status int= NULL OUT,
	@message Varchar(MAX)=NULL OUT
	 
AS
	BEGIN 
		
		DECLARE @ErrorMessage varchar(MAX)

			IF @Command='login'
				BEGIN
					
					BEGIN TRANSACTION

					BEGIN TRY 
						if exists(select L.Username ,
								L.Password
						 from tbllogin as L
						 where Username=@username  and                                                                      Password=@password) 
	                     Begin
							  set @status=1
		                      set @message='valid user'
							  
							  -- update column with random alphanumeric
							
							 update tbllogin
							  set  Token =left(stuff(convert(varchar(36),newid()),9,1,''),10),
								   Cookie = left(stuff(convert(varchar(36),newid()),9,1,''),10)
								where UserName = @UserName AND Password=@Password
       							   	                       
						    End
						
						else
						Begin
						set @status=0
						 set @message='invalid user'
						End
						

						COMMIT TRANSACTION
					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						

						RAISERROR(@ErrorMessage,16,1)
					END CATCH

				END
				End

				GO

				--exec uspSetUserLogin @Command = login, @Username = mangal , @Password= mangal123;