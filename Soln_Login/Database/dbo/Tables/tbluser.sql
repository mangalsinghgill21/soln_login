﻿CREATE TABLE [dbo].[tbluser] (
    [Userid]    NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [Firstname] NCHAR (10)   NOT NULL,
    [Lastname]  NCHAR (10)   NOT NULL,
    CONSTRAINT [PK_tbluser] PRIMARY KEY CLUSTERED ([Userid] ASC)
);

