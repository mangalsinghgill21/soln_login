﻿using Abstract.Modules;
using Dal.Repository.FactoryRepository;
using Dal.Repository.IUserRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserEntity.Interface;

namespace Dal.RepositoryFacade
{
   public class UserRepositoryFacade : UserAbstract
    {
       #region Declaration
     
        private Lazy<ILoginuserrepository> loginusererrepositoryobj = null;
#endregion
       
        #region Constructor
        public UserRepositoryFacade()
        {
           
        }
        #endregion

      
        #region Private Property (Sub Repository Instance)
       
        private Lazy<ILoginuserrepository> UserLoginRepositoryInstance
        {
            get
            {
                return
                        loginusererrepositoryobj
                            ??
                                (
                                    loginusererrepositoryobj = new Lazy<ILoginuserrepository>(() => UserRepositoryFactory.ExecuteFactory<ILoginuserrepository>(UserRepositoryFactory.RepositoryType.LoginUserRepository)));
            }
        }

        
        #endregion

        #region Public Method
   

        public async override Task<dynamic> userlogin(Iuserentity userentity)
        {
            try
            {
                return await this.UserLoginRepositoryInstance
                    .Value
                    .userlogin(userentity);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public override void Dispose()
        {
                       
        }
        #endregion
    }
}
