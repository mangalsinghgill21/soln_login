﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserEntity.Interface;

namespace Dal.Repository.IUserRepository
{
   public interface ILoginuserrepository:IDisposable
    {
       Task<dynamic> userlogin(Iuserentity userentity);
    }
}
