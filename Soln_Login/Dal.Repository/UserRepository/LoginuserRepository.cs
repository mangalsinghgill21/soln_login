﻿using Dal.ORM.Factory;
using Dal.ORM.Interface.IConcrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserEntity.Interface;

namespace Dal.Repository.UserRepository
{
   public class LoginuserRepository
    {
       #region Declaration
        private Lazy<Iuserconcrete> userconcreteobj = null;

        private static Lazy<LoginuserRepository>  loginuserrepositoryobj = null;
        #endregion

        #region Constructor
        private LoginuserRepository()
        {

        }
        #endregion

        #region Property
        private Lazy<Iuserconcrete> UserConcreteInstance
        {
            get
            {
                return
                        userconcreteobj
                            ??
                                (userconcreteobj = new Lazy<Iuserconcrete>(() => FactoryConcrete.ExecuteFactory<Iuserconcrete>(FactoryConcrete.ConcreteType.Userlogin)));
            }
        }

        public static Lazy<LoginuserRepository> CreateInstance
        {
            get
            {
                return
                       loginuserrepositoryobj
                            ??
                                (loginuserrepositoryobj = new Lazy<LoginuserRepository>(() => new LoginuserRepository()));
            }
        }
        #endregion 



        public async Task<dynamic> UserLogin(Iuserentity userentity)
        {
            int? status = null;
            string message = null;
           try
            {
                
                     await  UserConcreteInstance
                       .Value
                       .SetDataAsync("Login",
                       userentity,
                       (leStatus, leMessage) => {
                           status = leStatus;
                           message = leMessage;
                       });

                return (status == 1) ? (dynamic)true : (dynamic)message;
            }
            catch(Exception)
            {
                throw;
            }
        }

        public void Dispose()
        {
            userconcreteobj.Value.Dispose();

            loginuserrepositoryobj = null;
        }
    }

  }

