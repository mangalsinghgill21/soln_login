﻿using Dal.Repository.IUserRepository;
using Dal.Repository.UserRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Repository.FactoryRepository
{
   public static class UserRepositoryFactory
    {
        #region enum 
        public enum RepositoryType
        {
            LoginUserRepository=0
            
        };
        #endregion

        #region Declaration
        private static Dictionary<RepositoryType, dynamic> dicObj = new Dictionary<RepositoryType, dynamic>();
        #endregion

        #region Constructor
        static UserRepositoryFactory()
        {
            AddRepositoryInstance();
        }
        #endregion

        #region Private Method
        private static void AddRepositoryInstance()
        {
            
            dicObj.Add(RepositoryType.LoginUserRepository, new Lazy<ILoginuserrepository>(() => (dynamic)LoginuserRepository.CreateInstance.Value));
         
        }
        #endregion

        #region Public Method
        public static TRepository ExecuteFactory<TRepository>(RepositoryType repositoryTypeObj) where TRepository : class
        {
            return dicObj[repositoryTypeObj].Value as TRepository;
        }
        #endregion 
    }
}
