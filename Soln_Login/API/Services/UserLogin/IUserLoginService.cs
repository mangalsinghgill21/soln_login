﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using UserEntity;

namespace API.Services.UserLogin
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUserLoginService" in both code and config file together.
    [ServiceContract]
    public interface IUserLoginService
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UserLoginAsync")]

        [ServiceKnownType(typeof(userentity))]
        Task<object> UserLoginAsync(userentity userentityobj);


    }
}
