﻿using Bal.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using UserEntity;
using System.Threading.Tasks;

namespace API.Services.UserLogin
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UserLoginService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select UserLoginService.svc or UserLoginService.svc.cs at the Solution Explorer and start debugging.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class UserLoginService : IUserLoginService
    {

        public async Task<object> UserLoginAsync(userentity userentityobj)
        {


            try
            {
                UserContext usercontextobj = new UserContext();
                return await usercontextobj.UserLogin(userentityobj);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
