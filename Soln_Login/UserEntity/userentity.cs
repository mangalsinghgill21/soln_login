﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserEntity.Interface;

namespace UserEntity
{
    public class userentity : Iuserentity
    {
        public decimal Userid { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
        public dynamic Token { get; set; }
        public dynamic Cookies { get; set; }
    }
}
